# Description:
#   Checks if two URIs have a sameAs relation
#
# Commands:
#   hubot is <uri> the same as <uri>?

fs = require 'fs'

module.exports = (robot) ->

	robot.respond /is (.*) the same as (.*)/i, (msg) ->
		uri1 = msg.match[1]
		uri2 = msg.match[2]
		
		if uri1 is uri2
			msg.reply "Well duh."

		else
			if uri1.search /dbpedia.org\/resource/i > 0
				d_uri = uri1
				other = uri2
				i = uri1.search /dbpedia.org\/resource/i
			else if uri2.search /dbpedia.org\/resource/i > 0
				d_uri = uri2
				other = uri1
				i = uri2.search /dbpedia.org\/resource/i
			else
				msg.reply "I need at least one dbpedia URI to do anything useful right now."

			start = i + 21
			thing = d_uri.substr start, d_uri.length

			url = "http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=select*%7Bdbpedia%3A" + thing + "+owl%3AsameAs+%3FsameAs%7D&format=json"

			msg.reply url

			#https = require 'https'

			#robot.http(url)
			#	.header('Accept', 'application/json')
			#	.get() (err, res, body) ->
			#		if err
			#			msg.reply "DBPedia is having a moment."
			#			return
			#		msg.reply res.statusCode

			# FAKE IT - this is what you get back from dbpedia
			body = {
				"head": {
					"link": [],
					"vars": [
						"sameAs"
					]
				},
				"results": {
					"distinct": false,
					"ordered": true,
					"bindings": [
						{
							"sameAs": {
								"type": "uri",
								"value": "http://rdf.freebase.com/ns/m.0d9525"
							}
						},
						{
							"sameAs": {
								"type": "uri",
								"value": "http://www.wikidata.org/entity/Q5552825"
							}
						},
						{
							"sameAs": {
								"type": "uri",
								"value": "http://yago-knowledge.org/resource/Gerry_Kelly"
							}
						}
					]
				}
			}

			# I don't really understand what's going on here, I'm sure there shouldn't be so many for loops.
			r = (sa for sa in body.results.bindings)
			uris = []
			for k, v  of r
				for i, j of v
					for x, y of j
						uris.push y.value
			for k, uri of uris # HERENOW wat???
				msg.reply uri


